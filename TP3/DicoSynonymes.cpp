/**
* \file DicoSynonymes.cpp
* \brief Le code des opérateurs du DicoSynonymes.
* \author Jonathan Plourde
* \version 0.1
* \date juillet 2014
*
* Travail pratique numéro 3.
*
*/

#include "DicoSynonymes.h"
#include "AssertionException.h"
#include <stdexcept>
#include <numeric>
#include <algorithm>

namespace TP3
{

	// Méthode fournie que vous pouvez modifier si vous voulez
	void DicoSynonymes::chargerDicoSynonyme(std::ifstream& fichier)
	{
		ASSERTION(fichier.is_open());

		std::string ligne;
		std::string buffer;
		std::string radical;
		int cat = 1;

		while (std::getline(fichier, ligne)) // tant qu'on peut lire dans le fichier
		{

			if (ligne == "$")
			{
				cat = 3;
				std::getline(fichier, ligne);
			}
			if (cat == 1)
			{
				radical = ligne;
				ajouterRadical(radical);
				cat = 2;
			}
			else if (cat == 2)
			{
				std::stringstream ss(ligne);
				while (ss >> buffer)
					ajouterFlexion(radical, buffer);
				cat = 1;
			}
			else
			{
				std::stringstream ss(ligne);
				ss >> radical;
				ss >> buffer;
				int position = -1;
				ajouterSynonyme(radical, buffer, position);
				while (ss >> buffer)
					ajouterSynonyme(radical, buffer, position);
			}
		}
	}

	//Mettez l'implantation des autres méthodes demandées ici.

	/**
	 *  \fn DicoSynonymes::DicoSynonymes()
	 *  \brief Constructeur par défaut.
	 *  \post Une instance de la classe DicoSynonymes est initialisée.
	 */
	DicoSynonymes::DicoSynonymes():racine(0),nbRadicaux(0){

	}

	/**
	 *  \fn DicoSynonymes::DicoSynonymes(std::ifstream &fichier)
	 *  \brief Constructeur avec paramètres
	 * 	\param[in] fichier, fichier texte contenant le dictionnaire
	 *  \post Une instance de la classe DicoSynonymes est initialisée avec un fichier de dictionnaire de synonyme.
	 */
	DicoSynonymes::DicoSynonymes(std::ifstream &fichier):racine(0), nbRadicaux(0){

		chargerDicoSynonyme(fichier);

	}

	/**
	 *  \fn DicoSynonymes::~DicoSynonymes()
	 *  \brief Destructeur.
	 *  \post L'instance de DicoSynonymes est détruite.
	 */
	DicoSynonymes::~DicoSynonymes(){

		_Detruire(racine);

	}

	/*
	 * \fn void _auxDetruire(NoeudDicoSynonymes *& noeud)
	 * \brief Détruire un arbre.
	 *
	 * \param[in] noeud, le noeud source pour la desctruction de l'arbre.
	 * \post L'instance de DicoSynonymes est détruite.
	 */
	void DicoSynonymes::_Detruire(NoeudDicoSynonymes *&noeud){

		if (noeud != nullptr) {
			_Detruire(noeud->gauche);
			_Detruire(noeud->droit);
			delete noeud;
			noeud = 0;
		}

	}

	/*
	 * \fn void DicoSynonymes::ajouterRadical(const std::string& motRadical)
	 * \brief Ajouter un radical au dictionnaire des synonymes tout en s’assurant de maintenir l'équilibre de l'arbre.
	 *
	 * \param[in] motRadical, le radical à ajouter.
	 * \post Un radical est ajouté au dictionnaire des synonymes.
	 * \exception Erreur si le radical existe déjà.
	 */
	void DicoSynonymes::ajouterRadical(const std::string& motRadical){

		//ASSERTION(true); //radical existe déjà
		_ajouterRadical(racine, motRadical);

	}

	/*
	 * \fn void DicoSynonymes::_ajouterRadical(NoeudDicoSynonymes *&noeud, std::string radical)
	 * \brief Méthode récursive qui permet d'ajouter un radical au dictionnaire des synonymes tout en s’assurant de maintenir l'équilibre de l'arbre.
	 *
	 * \param[in] noeud, l'arbre où l'on veux inséré un nouvel élément.
	 * \param[in] motRadical, le radical à ajouter.
	 * \post Un radical est ajouté au dictionnaire des synonymes.
	 * \exception Erreur si le radical existe déjà.
	 */
	void DicoSynonymes::_ajouterRadical(NoeudDicoSynonymes *&noeud, std::string radical){

		if(noeud == nullptr){
			noeud = new NoeudDicoSynonymes(radical);
			nbRadicaux++;
			return;
		}else if(noeud->radical < radical){
			_ajouterRadical(noeud->droit, radical);
		}else if(noeud->radical > radical){
			_ajouterRadical(noeud->gauche, radical);
		}else{
			throw std::logic_error("Le radical existe déjà");
		}

		//_miseAJourHauteurNoeud(noeud);
		_balancer(noeud);

	}

	/*
	 * \fn void DicoSynonymes::_balancer(NoeudDicoSynonymes *&noeud)
	 * \brief Méthode récursive qui permet de balancer un arbre.
	 *
	 * \param[in] noeud, l'arbre où l'on veux inséré un nouvel élément.
	 * \post L'arbre à été balancé et les hauteurs mise à jour.
	 */
	void DicoSynonymes::_balancer(NoeudDicoSynonymes *&noeud){

		if(noeud == nullptr){
			return; // Ne rien faire
		}

		if(_debalancementAGauche(noeud)){
			if(_sousArbrePencheADroite(noeud->gauche)){
				_zigZagGauche(noeud);
			}else{
				_zigZigGauche(noeud);
			}
		}else if(_debalancementADroite(noeud)){
			if(_sousArbrePencheAGauche(noeud->droit)){
				_zigZagDroit(noeud);
			}else{
				_zigZigDroit(noeud);
			}
		}else{
			_miseAJourHauteurNoeud(noeud);
		}

	}

	/*
	 * \fn int DicoSynonymes::_hauteur(NoeudDicoSynonymes *&noeud) const
	 * \brief Permet de savoir la hauteur d'un arbre.
	 *
	 * \return un entier représentant la hauteur d'un noeud.
	 */
	int DicoSynonymes::_hauteur(NoeudDicoSynonymes *&noeud) const{

		if(noeud == nullptr){
			return -1;
		}
		return noeud->hauteur;

	}

	/*
	 * \fn void DicoSynonymes::_miseAJourHauteurNoeud(NoeudDicoSynonymes *&noeud)
	 * \brief Méthode qui permet de mettre à jour la hauteur d'un noeud.
	 *
	 * \param[in] noeud, noeud à mettre à jour.
	 * \post La hauteur du noeud est à jour.
	 */
	void DicoSynonymes::_miseAJourHauteurNoeud(NoeudDicoSynonymes *&noeud){

		if(noeud != nullptr) {
				noeud->hauteur = 1 + std::max(_hauteur(noeud->gauche), _hauteur(noeud->droit));
		}

	}

	/*
	 * \fn bool DicoSynonymes::_debalancementAGauche(Noe<numeric>udDicoSynonymes *&noeud) const
	 * \brief Permet de savoir si un arbre est débalancé à gauche.
	 *
	 * \return Un booléen, vrai si le neoud est débalancé à gauche, faux sinon.
	 */
	bool DicoSynonymes::_debalancementAGauche(NoeudDicoSynonymes *&noeud) const{

		if(noeud == nullptr){
			return false;
		}

		return _hauteur(noeud->gauche) - _hauteur(noeud->droit) > 1;

	}

	/*
	 * \fn bool DicoSynonymes::_debalancementADroite(NoeudDicoSynonymes *&noeud) const
	 * \brief Permet de savoir si un arbre est débalancé à droite.
	 *
	 * \return Un booléen, vrai si le noeud est débalancé à droite, faux sinon.
	 */
	bool DicoSynonymes::_debalancementADroite(NoeudDicoSynonymes *&noeud) const{

		if(noeud == nullptr){
			return false;
		}

		return _hauteur(noeud->droit) - _hauteur(noeud->gauche) > 1;

	}

	/*
	 * \fn bool DicoSynonymes::_sousArbrePencheAGauche(NoeudDicoSynonymes *&noeud) const
	 * \brief Permet de savoir si le noeud sous critique penche a gauche.
	 *
	 * \return Un booléen, vrai si le noeud sous critique penche à gauche, faux sinon.
	 */
	bool DicoSynonymes::_sousArbrePencheAGauche(NoeudDicoSynonymes *&noeud) const{

		if(noeud == nullptr){
			return false;
		}

		return _hauteur(noeud->gauche) > _hauteur(noeud->droit);

	}

	/*
	 * \fn bool DicoSynonymes::_sousArbrePencheADroite(NoeudDicoSynonymes *&noeud) const
	 * \brief Permet de savoir si le noeud sous critique penche a droite.
	 *
	 * \return Un booléen, vrai si le noeud sous critique penche à droite, faux sinon.
	 */
	bool DicoSynonymes::_sousArbrePencheADroite(NoeudDicoSynonymes *&noeud) const{

		if(noeud == nullptr){
			return false;
		}

		return _hauteur(noeud->gauche) < _hauteur(noeud->droit);

	}

	/*
	 * \fn void DicoSynonymes::_zigZigGauche(NoeudDicoSynonymes *&noeud)
	 * \brief Méthode qui permet de faire une rotation vert la gauche d'un arbre.
	 *
	 * \param[in] noeud, l'arbre où l'on veux faire la rotation.
	 * \post L'arbre à subit une rotation à gauche.
	 */
	void DicoSynonymes::_zigZigGauche(NoeudDicoSynonymes *&noeud){

		NoeudDicoSynonymes *noeudSC = noeud->gauche;
		noeud->gauche = noeudSC->droit;
		noeudSC->droit = noeud;

		// Ajuster la hauteur des noeuds
		_miseAJourHauteurNoeud(noeud);
		_miseAJourHauteurNoeud(noeudSC);

		noeud = noeudSC;

	}

	/*
	 * \fn void DicoSynonymes::_zigZigDroite(NoeudDicoSynonymes *&noeud)
	 * \brief Méthode qui permet de faire une rotation vert la droite d'un arbre.
	 *
	 * \param[in] noeud, l'arbre où l'on veux faire la rotation.
	 * \post L'arbre à subit une rotation à droite.
	 */
	void DicoSynonymes::_zigZigDroit(NoeudDicoSynonymes *&noeud){

		NoeudDicoSynonymes *noeudSC = noeud->droit;
		noeud->droit = noeudSC->gauche;
		noeudSC->gauche = noeud;

		// Ajuster la hauteur des noeuds
		_miseAJourHauteurNoeud(noeud);
		_miseAJourHauteurNoeud(noeudSC);

		noeud = noeudSC;

	}

	/*
	 * \fn void DicoSynonymes::_zigZagGauche(NoeudDicoSynonymes *&noeud)
	 * \brief Méthode qui permet de faire une rotation vert la droite du noeud sous critique et une rotation vert la gauche d'un arbre.
	 *
	 * \param[in] noeud, l'arbre où l'on veux faire la rotation.
	 * \post L'arbre à subit une rotation à droite du noeud sous critique et une rotation vert la gauche d'un arbre.
	 */
	void DicoSynonymes::_zigZagGauche(NoeudDicoSynonymes *&noeud){

		_zigZigDroit(noeud->gauche);
		_zigZigGauche(noeud);

	}

	/*
	 * \fn void DicoSynonymes::_zigZagDroite(NoeudDicoSynonymes *&noeud)
	 * \brief Méthode qui permet de faire une rotation vert la gauche du noeud sous critique et une rotation vert la droite d'un arbre.
	 *
	 * \param[in] noeud, l'arbre où l'on veux faire la rotation.
	 * \post L'arbre à subit une rotation à gauche du noeud sous critique et une rotation vert la droite d'un arbre.
	 */
	void DicoSynonymes::_zigZagDroit(NoeudDicoSynonymes *&noeud){

		_zigZigGauche(noeud->droit);
		_zigZigDroit(noeud);

	}

	/*
	 * \fn DicoSynonymes::ajouterFlexion(const std::string& motRadical, const std::string& motFlexion)
	 * \brief Ajouter une flexion (motFlexion) d'un radical (motRadical) à sa liste de flexions.
	 *
	 * \param[in] motRadical, le radical à qui l'on ajoutre une flexion.
	 * \param[in] motFlexion, la flexion à ajouter au radical.
	 * \post Une flexion est ajouté au radical du dictionnaire des synonymes.
	 * \exception Erreur si motFlexion existe déjà ou motRadical n'existe pas.
	 */
	void DicoSynonymes::ajouterFlexion(const std::string& motRadical, const std::string& motFlexion){

		NoeudDicoSynonymes *noeudF = nullptr;
		noeudF = _trouverRadical(motRadical, racine);

		if(noeudF == nullptr){
			throw std::logic_error("Le radical n'existe pas");
		}else{
			if(std::find(std::begin(noeudF->flexions), std::end(noeudF->flexions), motFlexion) == std::end(noeudF->flexions)){
				//test
				//noeudF->flexions.resize(noeudF->flexions.size()+1);
				//
				noeudF->flexions.push_back(motFlexion);
			}else{
				throw std::logic_error("la flexion existe déjà");
			}
		}

	}

	/*
	 * \fn void ajouterSynonyme(const std::string& motRadical, const std::string& motSynonyme, int& numGroupe)
	 * \brief Ajouter un synonyme (motSynonyme) d'un radical (motRadical) à un de ses groupes de synonymes.
	 *
	 * \param[in] motRadical, le radical à qui l'on ajoutre un synonyme.
	 * \param[in] motSynonyme, le synonyme à ajouter.
	 * \param[in] numGroupe, le numéro du groupe à ajouter le synonyme, mais si numGroupe vaut –1, le synonyme est ajouté dans un nouveau groupe de synonymes et retourne le numéro de ce nouveau groupe dans numgroupe par référence.
	 * \post Une flexion est ajouté au radical du dictionnaire des synonymes.
	 * \exception Erreur si motSynonyme est déjà dans la liste des synonymes du motRadical.
	 * \exception Erreur si numGroupe n'est pas correct ou motRadical n'existe pas.
	 */
	void DicoSynonymes::ajouterSynonyme(const std::string& motRadical, const std::string& motSynonyme, int& numGroupe){

		NoeudDicoSynonymes *noeudF = nullptr;
		NoeudDicoSynonymes *synonymeF = nullptr;

		noeudF = _trouverRadical(motRadical, racine);
		synonymeF = _trouverRadical(motSynonyme, racine);

		if(noeudF == nullptr){
			throw std::logic_error("Le radical n'existe pas");
		}

		if(synonymeF == nullptr){
			this->ajouterRadical(motSynonyme);

			//on recherche le nouveau synonyme ajouté
			synonymeF = _trouverRadical(motSynonyme, racine);
			//throw std::logic_error("Le synonyme n'existe pas");
		}

		if(numGroupe == -1){
			size_t iEnd = groupesSynonymes.size();
			groupesSynonymes.resize(iEnd +1);
			numGroupe = groupesSynonymes.size() - 1;
			noeudF->appSynonymes.push_back(numGroupe);
		}

		if(std::find(std::begin(groupesSynonymes[numGroupe]), std::end(groupesSynonymes[numGroupe]), synonymeF) == std::end(groupesSynonymes[numGroupe])){
			groupesSynonymes[numGroupe].push_back(synonymeF);
		}else{
			//throw std::logic_error("le synonyme existe déjà");
		}

	}

	/*
	 * \fn void DicoSynonymes::supprimerRadical(const std::string& motRadical)
	 * \brief Supprimer un radical du dictionnaire des synonymes tout en s’assurant de maintenir l'équilibre de l'arbre.
	 *
	 * \param[in] motRadical, le radical que l'on veux suprimmer.
	 * \post Un radical est ajouté au dictionnaire des synonymes.
	 * \exception Erreur si l'arbre est vide ou motRadical n'existe pas.
	 */
	void DicoSynonymes::supprimerRadical(const std::string &motRadical){

		ASSERTION(!estVide());
		_supprimerRadical(racine, motRadical);

	}

	void DicoSynonymes::_supprimerRadical(NoeudDicoSynonymes *&noeud, const std::string &motRadical){

		if(noeud == nullptr){
			throw std::logic_error("Le radical n'existe pas");
		}

		if (noeud->radical > motRadical){
			_supprimerRadical(noeud->gauche, motRadical);
		}else if(noeud->radical < motRadical){
			_supprimerRadical(noeud->droit, motRadical);
		}else if(noeud->gauche != 0 && noeud->droit != 0){
			// 3e cas: recherche du successeur minimal
			NoeudDicoSynonymes *temp = noeud->droit;
			NoeudDicoSynonymes *parent = noeud;
			while(temp->gauche != 0){
				parent = temp;
				temp = temp->gauche;
			}
			noeud->radical = temp->radical;
			_supprimerRadical(parent->gauche, temp->radical);
		}else{
			// Cas 1 et 2. Cas simple.
			NoeudDicoSynonymes *vieuxNoeud = noeud;
			noeud = (noeud->gauche != 0) ? noeud->gauche : noeud->droit;
			delete vieuxNoeud;
			--nbRadicaux;
		}
		_balancer(noeud);
	}

	/*
	 * \fn void DicoSynonymes::supprimerFlexion(const std::string& motRadical, const std::string& motFlexion)
	 * \brief Supprimer une flexion (motFlexion) d'un radical (motRadical) de sa liste de flexions.
	 *
	 * \param[in] motRadical, le radical qui fait partie de l'arbre.
	 * \param[in] motFlexion, la flexionque l'on veux suprimmer.
	 * \post La flexion est enlevée du dictionnaire des synonymes.
	 * \exception Erreur si l'arbre est vide ou motFlexion n'existe pas ou motRadical n'existe pas.
	 */
	void DicoSynonymes::supprimerFlexion(const std::string& motRadical, const std::string& motFlexion){

		ASSERTION(!estVide());

		NoeudDicoSynonymes *noeudF = nullptr;
		noeudF = _trouverRadical(motRadical, racine);

		if(noeudF == nullptr){
			throw std::logic_error("Le radical n'existe pas.");
		}

		auto it = std::find(std::begin(noeudF->flexions),std::end(noeudF->flexions), motFlexion);
		if(it != std::end(noeudF->flexions)){
			noeudF->flexions.erase(it);
		}else{
			//throw std::logic_error("Le motFlexion n'existe pas.");
		}

	}

	/*
	 * \fn void supprimerSynonyme(const std::string& motRadical, const std::string& motSynonyme, int& numGroupe)
	 * \brief Retirer motSynonyme faisant partie du numéro de groupe numGroupe du motRadical.
	 *
	 * \param[in] motRadical, le radical qui fait partie de l'arbre.
	 * \param[in] motSynonyme, le synonyme à suprimmer.
	 * \param[in] numGroupe, le numéro du groupe où le synonyme sera suprimer
	 * \post Le synonyme est enlevé du dictionnaire des synonymes.
	 * \exception Erreur si motSynonyme ou motRadical ou numGroupe n'existent pas.
	 */
	void DicoSynonymes::supprimerSynonyme(const std::string& motRadical, const std::string& motSynonyme, int& numGroupe){

		NoeudDicoSynonymes *noeudF = nullptr;
		NoeudDicoSynonymes *synonymeF = nullptr;

		noeudF = _trouverRadical(motRadical, racine);
		synonymeF = _trouverRadical(motSynonyme, racine);

		if(noeudF == nullptr){
			throw std::logic_error("Le radical n'existe pas.");
		}

		if(synonymeF == nullptr){
			throw std::logic_error("Le radical n'existe pas.");
		}

		auto it = std::find(std::begin(noeudF->appSynonymes),std::end(noeudF->appSynonymes), numGroupe);
		if (it != std::end(noeudF->appSynonymes)){
			auto it2 = std::find(std::begin(groupesSynonymes[numGroupe]), std::end(groupesSynonymes[numGroupe]), synonymeF);
			if( it2 != std::end(groupesSynonymes[numGroupe])){
				groupesSynonymes[numGroupe].erase(it2);
			}else{
				//throw std::logic_error("le synonyme pas");
			}
		}else{
			//throw std::logic_error("le numGroupe n'esite pas");
		}

	}

	/*
	 * \fn bool estVide() const
	 * \brief Vérifier si le dictionnaire est vide.
	 *
	 * \return Un booléen, vrai si le dictionnaire est vide, faux sinon.
	 */
	bool DicoSynonymes::estVide() const{

		return nbRadicaux == 0;

	}

	/*
	 * \fn int nombreRadicaux() const
	 * \brief Retourne le nombre de radicaux dans le dictionnaire.
	 *
	 * \return un entier représentant le nombre de radicaux.
	 */
	int DicoSynonymes::nombreRadicaux() const{

		return nbRadicaux;

	}

	/*
	 * \fn std::string DicoSynonymes::rechercherRadical(const std::string& mot) const
	 * \brief Retourne le radical du mot entré en paramètre.
	 *
	 * \param[in] mot, mot recherché.
	 * \exception Erreur si l'arbre est vide.
	 * \exception si la flexion n'est pas dans la liste de flexions du radical
	 */
	std::string DicoSynonymes::rechercherRadical(const std::string& mot) const{

		ASSERTION(!estVide());
		std::vector<NoeudDicoSynonymes *> vNoeud;
		NoeudDicoSynonymes * bestNoeud = nullptr;
		float bestSim = 0;

		_listerEnOrdre(racine, vNoeud);

		for(auto it : vNoeud){
			float sim = similitude(it->radical, mot);
			if(sim > bestSim){
				bestSim = sim;
				bestNoeud = it;
			}
		}

		//si le mot est un radical
		if(bestNoeud->radical == mot){
			return bestNoeud->radical;
		}

		auto flex = std::find(std::begin(bestNoeud->flexions),std::end(bestNoeud->flexions), mot);
		if(flex != std::end(bestNoeud->flexions) && bestNoeud != nullptr){
			return bestNoeud->radical;
		}else{
			throw std::logic_error("Le radical n'existe pas");
		}

	}

	/*
	 * \fn void DicoSynonymes::_listerEnOrdre(NoeudDicoSynonymes * const &noeud, std::vector<NoeudDicoSynonymes *> &vNoeud) const
	 * \brief trie en ordre dans l'arbre
	 *
	 * \param[in] noeud, noeud de départ.
	 * \param[in] vNoeud, une vecteur de Noeud.
	 * \post vNoeud contient en ordre symetrique le dictionnaire.
	 */
	void DicoSynonymes::_listerEnOrdre(NoeudDicoSynonymes * const &noeud, std::vector<NoeudDicoSynonymes *> &vNoeud) const{

		if (noeud != nullptr) {
			_listerEnOrdre(noeud->gauche, vNoeud);
			vNoeud.push_back(noeud);
			_listerEnOrdre(noeud->droit, vNoeud);
		}

	}

	/*
	 * \fn float DicoSynonymes::similitude(const std::string& mot1, const std::string& mot2) const
	 * \brief Retourne un réel entre 0 et 1 qui représente le degré de similitude entre mot1 et mot2 où 0 représente deux mots complétement différents et 1 deux mots identiques.
	 *
	 * \param[in] mot1,mot2, mot à comparer.
	 * \return Réel entre 0 et 1 qui représente le degré de similitude.
	 */
	float DicoSynonymes::similitude(const std::string& mot1, const std::string& mot2) const{
		// SOURCE : https://github.com/miguelvps/c/blob/master/jarowinkler.c
		// Algorithme jarowinkler.

		float SCALING_FACTOR = 0.1;
    int i, j, l;
    int m = 0, t = 0;
    int sl = mot1.length();
    int al = mot2.length();
    int sflags[sl], aflags[al];
    int range = std::max(0, std::max(sl, al) / 2 - 1);
    double dw;

    if (!sl || !al)
    	return 0.0;

    for (i = 0; i < al; i++)
    	aflags[i] = 0;

    for (i = 0; i < sl; i++)
    	sflags[i] = 0;

    /* calculate matching characters */
    for (i = 0; i < al; i++) {
			for (j = std::max(i - range, 0), l = std::min(i + range + 1, sl); j < l; j++) {
				if (mot2[i] == mot1[j] && !sflags[j]) {
					sflags[j] = 1;
					aflags[i] = 1;
					m++;
					break;
				}
			}
    }

    if (!m){
			return 0.0;
    }

    /* calculate character transpositions */
    l = 0;
    for (i = 0; i < al; i++) {
			if (aflags[i] == 1) {
				for (j = l; j < sl; j++) {
					if (sflags[j] == 1) {
						l = j + 1;
						break;
					}
				}
					if (mot2[i] != mot1[j])
						t++;
        }
    }
    t /= 2;

    /* Jaro distance */
    dw = (((double)m / sl) + ((double)m / al) + ((double)(m - t) / m)) / 3.0;

    /* calculate common string prefix up to 4 chars */
    l = 0;
    for (i = 0; i < std::min(std::min(sl, al), 4); i++)
			if (mot1[i] == mot2[i])
				l++;

    /* Jaro-Winkler distance */
    dw = dw + (l * SCALING_FACTOR * (1 - dw));

    return dw;

	}

	/*
	 * \fn int DicoSynonymes::getNombreSens(std::string radical) const
	 * \brief Donne le nombre de groupes de synonymes d'un radical.
	 *
	 * \param[in] radical, radical recherché.
	 * \return Entier représentant le nombre de groupes de synonymes d'un radical.
	 */
	int DicoSynonymes::getNombreSens(std::string radical) const{

		NoeudDicoSynonymes *noeudF = nullptr;

		noeudF = _trouverRadical(radical, racine);
		return noeudF->appSynonymes.size();

	}

	/*
	 * \fn std::string getSens(std::string radical, int position) const
	 * \brief Donne le premier synonyme du groupe de synonymes d'un radical.
	 *
	 * \param[in] radical, radical recherché.
	 * \param[in] position, indice du groupe de de synonyme.
	 * \return Chaîne de caractère correspondant au premier synonyme est retourné.
	 */
	std::string DicoSynonymes::getSens(std::string radical, int position) const{

		NoeudDicoSynonymes *noeudF = nullptr;
		noeudF = _trouverRadical(radical, racine);

		if(noeudF == nullptr){
			throw std::logic_error("Le radical n'existe pas");
		}

		return (*groupesSynonymes[noeudF->appSynonymes[position]].begin())->radical;

	}

	/*
	 * \fn std::vector<std::string> getSynonymes(std::string radical, int position) const
	 * \brief Donne tous les synonymes d'un radical pour un groupe en particulier qui est identifé par position.
	 *
	 * \param[in] radical, radical recherché.
	 * \param[in] position, indice du groupe de de synonyme.
	 * \return Vecteur de chaîne de caractère contenant les synonymes d'un groupe.
	 */
	std::vector<std::string> DicoSynonymes::getSynonymes(std::string radical, int position) const{

		NoeudDicoSynonymes *noeudF = nullptr;
		noeudF = _trouverRadical(radical, racine);


		if(noeudF == nullptr){
			throw std::logic_error("Le radical n'existe pas");
		}

		auto it = std::find(std::begin(noeudF->appSynonymes),std::end(noeudF->appSynonymes), position);

		if(it != std::end(noeudF->appSynonymes)){
			auto it2 = groupesSynonymes[position].begin();
			std::vector<std::string> vSyn;

			for(; it2 != groupesSynonymes[position].end(); it2++){
				vSyn.push_back((*it2)->radical);
			}

			return vSyn;
		}

	}

	/*
	 * \fn std::vector<std::string> getFlexions(std::string radical) const const
	 * \brief Donne toutes les flexions d'un radical.
	 *
	 * \param[in] radical, radical recherché.
	 * \return Vecteur de chaîne de caractère contenant les flexions d'un radical.
	 */
	std::vector<std::string> DicoSynonymes::getFlexions(std::string radical) const{

		NoeudDicoSynonymes *noeudF = nullptr;
		std::vector<std::string> flexion;

		noeudF = _trouverRadical(radical, racine);

		if(noeudF == nullptr){
			throw std::logic_error("Le radical n'existe pas");
		}else{
			flexion.reserve(noeudF->flexions.size());
			std::copy(std::begin(noeudF->flexions), std::end(noeudF->flexions), std::back_inserter(flexion));
			return flexion;
		}

	}

	/*
	 * \fn DicoSynonymes::NoeudDicoSynonymes * DicoSynonymes::_trouverRadical(const std::string& motRadical, NoeudDicoSynonymes * const &noeud) const
	 * \brief Trouve un radical dans l'arbre et retourne sont adresse.
	 *
	 * \param[in] radical, radical recherché.
	 * \param[in] noeud, noeud de l'arbre où effectuer la recherche.
	 * \return L'adresse du noued trouvé, nullptr sinon.
	 */
	DicoSynonymes::NoeudDicoSynonymes * DicoSynonymes::_trouverRadical(const std::string& motRadical, NoeudDicoSynonymes * const &noeud) const{

		if(noeud == nullptr){
			return nullptr;
		}else if(noeud->radical == motRadical) {
			return noeud;
		}else if(noeud->radical > motRadical) {
			return _trouverRadical(motRadical, noeud->gauche);
		}else{
			return _trouverRadical(motRadical, noeud->droit);
		}

	}
}//Fin du namespace
